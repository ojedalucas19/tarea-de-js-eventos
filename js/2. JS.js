let vector = [10, 'Hola', true, 3.14, false, 'Mundo'];

// a. Imprimir en la consola cada valor usando "for"
console.log('Imprimir usando "for":');
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

// b. Idem al anterior usando "forEach"
console.log('Imprimir usando "forEach":');
vector.forEach((valor) => {
  console.log(valor);
});

// c. Idem al anterior usando "map"
console.log('Imprimir usando "map":');
vector.map((valor) => {
  console.log(valor);
});

// d. Idem al anterior usando "while"
console.log('Imprimir usando "while":');
let index = 0;
while (index < vector.length) {
  console.log(vector[index]);
  index++;
}

// e. Idem al anterior usando "for..of"
console.log('Imprimir usando "for..of":');
for (let valor of vector) {
  console.log(valor);
}
