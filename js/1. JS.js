let vector = [10, 'Hola', true, 3.14, false, 'Mundo'];

//a. Imprimir en la consola el vector
console.log(vector);

// b. Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log('Primer elemento:', vector[0]);
console.log('Último elemento:', vector[vector.length - 1]);

// c. Modificar el valor del tercer elemento
vector[2] = 'Adiós';

// d. Imprimir en la consola la longitud del vector
console.log('Longitud del vector:', vector.length);

// e. Agregar un elemento al vector usando "push"
vector.push(42);

// f. Eliminar elemento del final e imprimirlo usando "pop"
const ultimoElemento = vector.pop();
console.log('Último elemento eliminado:', ultimoElemento);

// g. Agregar un elemento en la mitad del vector usando "splice"
vector.splice(3, 0, 'OpenAI');

// h. Eliminar el primer elemento usando "shift"
const primerElemento = vector.shift();
console.log('Primer elemento eliminado:', primerElemento);

// i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primerElemento);

// vector actualizado
console.log(vector);
